import React from 'react';
import './AffineTransformations.css';

class AffineTransformationsPractice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            xVertexA: 0,
            yVertexA: 0,
            xVertexB: 0,
            yVertexB: 0,
            xVertexC: 0,
            yVertexC: 0,
            scalingCoefficient: 1,
            turnAngle: 0,
            displayStatusErrors: false
        };

        this.handleChangeXCoordinate = this.handleChangeXCoordinate.bind(this);
        this.handleChangeYCoordinate = this.handleChangeYCoordinate.bind(this);
        this.handleChangeAngleInput = this.handleChangeAngleInput.bind(this);
        this.handleSaveFileClick = this.handleSaveFileClick.bind(this);
        this.handleHelpButtonClick = this.handleHelpButtonClick.bind(this);
        this.handleCloseHelpButtonClick = this.handleCloseHelpButtonClick.bind(this);
        this.handleChangeScaling = this.handleChangeScaling.bind(this);
    }

    handleSaveFileClick() {
        var canvas = document.getElementById("canvasAffineTransformations");
        const link = document.createElement('a');
        link.download = 'affineTransformationImage.png';
        link.href = canvas.toDataURL();
        link.click();
    }

    handleHelpButtonClick() {
        var form = document.getElementById("affineTransformationsInputs");
        var helpBox = document.getElementsByClassName("helpBox")[0];
        var affineTransformationsBuilding = document.getElementById("affineTransformationsBuilding");
        var affineTransformationsProperties = document.getElementById("affineTransformationsProperties");
        var errors = document.getElementsByClassName("errorMessagesBox")[0];

        if (errors.style.getPropertyValue("display") === "block") {
            this.setState({displayStatusErrors: true});
        } else {
            this.setState({displayStatusErrors: false});
        }
        errors.style = "display: none;"
        affineTransformationsProperties.style = "width: 23%";
        affineTransformationsBuilding.style = "width: 76%";
        form.style = "display: none;"
        helpBox.style = "display: block;"
    }

    handleCloseHelpButtonClick() {
        var form = document.getElementById("affineTransformationsInputs");
        var helpBox = document.getElementsByClassName("helpBox")[0];
        var affineTransformationsBuilding = document.getElementById("affineTransformationsBuilding");
        var affineTransformationsProperties = document.getElementById("affineTransformationsProperties");
        var errors = document.getElementsByClassName("errorMessagesBox")[0];

        if (this.state.displayStatusErrors) {
            errors.style = "display: block;"
        } else {
            errors.style = "display: none;"
        }
        affineTransformationsProperties.style = "width: 18%";
        affineTransformationsBuilding.style = "width: 81%";
        form.style = "display: block;"
        helpBox.style = "display: none;"
    }

    handleChangeXCoordinate(e, vertex) {
        if (vertex === "A") {
            this.setState({xVertexA: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
            if (e.target.value > 10 || e.target.value < -10) {
                document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                    += "<li>Число " + e.target.value + " не належить проміжку [-10;10]</li>";
                document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
                return;
            }
            this.updateCanvas(
                isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
                this.state.yVertexA,
                this.state.xVertexB,
                this.state.yVertexB,
                this.state.xVertexC,
                this.state.yVertexC,
                this.state.scalingCoefficient,
                this.state.turnAngle
            );
        } else if (vertex === "B") {
            this.setState({xVertexB: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
            if (e.target.value > 10 || e.target.value < -10) {
                document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                    += "<li>Число " + e.target.value + " не належить проміжку [-10;10]</li>";
                document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
                return;
            }
            this.updateCanvas(
                this.state.xVertexA,
                this.state.yVertexA,
                isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
                this.state.yVertexB,
                this.state.xVertexC,
                this.state.yVertexC,
                this.state.scalingCoefficient,
                this.state.turnAngle
            );
        } else if (vertex === "C") {
            this.setState({xVertexC: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
            if (e.target.value > 10 || e.target.value < -10) {
                document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                    += "<li>Число " + e.target.value + " не належить проміжку [-10;10]</li>";
                document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
                return;
            }
            this.updateCanvas(
                this.state.xVertexA,
                this.state.yVertexA,
                this.state.xVertexB,
                this.state.yVertexB,
                isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
                this.state.yVertexC,
                this.state.scalingCoefficient,
                this.state.turnAngle
            );
        }
    }

    handleChangeYCoordinate(e, vertex) {
        if (vertex === "A") {
            this.setState({yVertexA: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
            if (e.target.value > 10 || e.target.value < -10) {
                document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                    += "<li>Число " + e.target.value + " не належить проміжку [-10;10]</li>";
                document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
                return;
            }
            this.updateCanvas(
                this.state.xVertexA,
                isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
                this.state.xVertexB,
                this.state.yVertexB,
                this.state.xVertexC,
                this.state.yVertexC,
                this.state.scalingCoefficient,
                this.state.turnAngle
            );
        } else if (vertex === "B") {
            this.setState({yVertexB: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
            if (e.target.value > 10 || e.target.value < -10) {
                document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                    += "<li>Число " + e.target.value + " не належить проміжку [-10;10]</li>";
                document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
                return;
            }
            this.updateCanvas(
                this.state.xVertexA,
                this.state.yVertexA,
                this.state.xVertexB,
                isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
                this.state.xVertexC,
                this.state.yVertexC,
                this.state.scalingCoefficient,
                this.state.turnAngle
            );
        } else if (vertex === "C") {
            this.setState({yVertexC: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
            if (e.target.value > 10 || e.target.value < -10) {
                document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                    += "<li>Число " + e.target.value + " не належить проміжку [-10;10]</li>";
                document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
                return;
            }
            this.updateCanvas(
                this.state.xVertexA,
                this.state.yVertexA,
                this.state.xVertexB,
                this.state.yVertexB,
                this.state.xVertexC,
                isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
                this.state.scalingCoefficient,
                this.state.turnAngle
            );
        }
    }

    handleChangeScaling(e) {
        this.setState({scalingCoefficient: isNaN(Number.parseFloat(e.target.value)) ? 0 : Number.parseFloat(e.target.value)});
        this.updateCanvas(
            this.state.xVertexA,
            this.state.yVertexA,
            this.state.xVertexB,
            this.state.yVertexB,
            this.state.xVertexC,
            this.state.yVertexC,
            isNaN(Number.parseFloat(e.target.value)) ? 0 : Number.parseFloat(e.target.value),
            this.state.turnAngle
        );
    }

    handleChangeAngleInput(e) {
        this.setState({turnAngle: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
        if (e.target.value > 360 || e.target.value < 0) {
            document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                += "<li>Число " + e.target.value + " не належить проміжку [0;360]</li>";
            document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
            return;
        }
        this.updateCanvas(
            this.state.xVertexA,
            this.state.yVertexA,
            this.state.xVertexB,
            this.state.yVertexB,
            this.state.xVertexC,
            this.state.yVertexC,
            this.state.scalingCoefficient,
            isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)
        );
    }

    drawCoordinateSystem(width, height, gridSize, lineWidth) {
        const ctx = document.getElementById("canvasAffineTransformations").getContext('2d');
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, width, height);
        ctx.strokeStyle = "grey";
        ctx.lineWidth = lineWidth;

        let rowsCount = height / (gridSize + lineWidth);
        let colsCount = width / (gridSize + lineWidth);

        for (let i = 0; i < colsCount; i++) {
            ctx.beginPath();
            ctx.moveTo(i * (gridSize + lineWidth), 0);
            ctx.lineTo(i * (gridSize + lineWidth), height);
            ctx.stroke();
        }
        for (let i = 0; i < rowsCount; i++) {
            ctx.beginPath();
            ctx.moveTo(0, i * (gridSize + lineWidth));
            ctx.lineTo(width, i * (gridSize + lineWidth));
            ctx.stroke();
        }

        ctx.lineWidth = lineWidth + 1;
        ctx.strokeStyle = "black";
        let OxCoordY = Math.round(rowsCount / 2) * (gridSize + lineWidth)
        let OyCoordX = Math.round(colsCount / 2) * (gridSize + lineWidth);
        ctx.beginPath();
        ctx.moveTo(OyCoordX, 0);
        ctx.lineTo(OyCoordX, height);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(0, OxCoordY);
        ctx.lineTo(width, OxCoordY);
        ctx.stroke();

        ctx.font = "15px Arial";
        ctx.fillStyle = "black";
        for (let i = 0; i < colsCount; i++) {
            ctx.beginPath();
            ctx.moveTo(i * (gridSize + lineWidth), OxCoordY - 5);
            ctx.lineTo(i * (gridSize + lineWidth), OxCoordY + 5);
            ctx.stroke();
            if (Math.round(colsCount / 2) !== i && i !== 0) {
                if (i > Math.round(colsCount / 2)) {
                    ctx.fillText((i - Math.round(colsCount / 2)).toString(),
                        i * (gridSize + lineWidth) - 8, OxCoordY + 20);
                } else {
                    ctx.fillText((i - Math.round(colsCount / 2)).toString(),
                        i * (gridSize + lineWidth) - 8, OxCoordY - 10);
                }
            }
        }
        for (let i = 0; i < rowsCount; i++) {
            ctx.beginPath();
            ctx.moveTo(OyCoordX - 5, i * (gridSize + lineWidth));
            ctx.lineTo(OyCoordX + 5, i * (gridSize + lineWidth));
            ctx.stroke();
            if (Math.round(rowsCount / 2) !== i && i !== 0) {
                if (i > Math.round(colsCount / 2)) {
                    if (Math.round(rowsCount / 2) - i < -9) {
                        ctx.fillText((Math.round(rowsCount / 2) - i).toString(),
                            OyCoordX - 30, i * (gridSize + lineWidth) + 5);
                    } else {
                        ctx.fillText((Math.round(rowsCount / 2) - i).toString(),
                            OyCoordX - 21, i * (gridSize + lineWidth) + 5);
                    }
                } else {
                    ctx.fillText((Math.round(rowsCount / 2) - i).toString(),
                        OyCoordX + 10, i * (gridSize + lineWidth) + 5);
                }
            }
        }
    }

    checkIfTriangleExist(a, b, c) {
        if ((a + b) <= c) {
            return false;
        }
        if ((b + c) <= a) {
            return false;
        }
        if ((a + c) <= b) {
            return false;
        }
        return true;
    }

    updateCanvas(xVertexA, yVertexA, xVertexB, yVertexB, xVertexC, yVertexC,
                 scalingCoefficient, turnAngle) {
        let a = Math.sqrt(Math.pow(xVertexB - xVertexA, 2) + Math.pow(yVertexB - yVertexA, 2));
        let b = Math.sqrt(Math.pow(xVertexC - xVertexA, 2) + Math.pow(yVertexC - yVertexA, 2));
        let c = Math.sqrt(Math.pow(xVertexB - xVertexC, 2) + Math.pow(yVertexB - yVertexC, 2));
        if (!this.checkIfTriangleExist(a, b, c)) {
            document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                += "<li>Трикутника з такими вершинами не існує.</li>";
            document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
            return;
        }

        const ctx = document.getElementById("canvasAffineTransformations").getContext('2d');
        let width = 700;
        let height = 700;
        let systemLineWidth = 1;
        let lineWidth = 2;
        let gridSize = 30;


        ctx.clearRect(0, 0, width, height);
        this.drawCoordinateSystem(width, height, gridSize, systemLineWidth);
        ctx.strokeStyle = "blue";
        ctx.lineWidth = lineWidth;
        let xVertexBNew = 0;
        let yVertexBNew = 0;
        let xVertexCNew = 0;
        let yVertexCNew = 0;

        if (xVertexB > 0) {
            xVertexBNew = xVertexB + Math.abs((xVertexB - xVertexA)) * (scalingCoefficient - 1);
        } else {
            xVertexBNew = xVertexB - Math.abs((xVertexB - xVertexA)) * (scalingCoefficient - 1);
        }
        if (yVertexB > 0) {
            yVertexBNew = yVertexB + Math.abs((yVertexB - yVertexA)) * (scalingCoefficient - 1);
        } else {
            yVertexBNew = yVertexB - Math.abs((yVertexB - yVertexA)) * (scalingCoefficient - 1);
        }
        if (xVertexC > 0) {
            xVertexCNew = xVertexC + Math.abs((xVertexC - xVertexA)) * (scalingCoefficient - 1);
        } else {
            xVertexCNew = xVertexC - Math.abs((xVertexC - xVertexA)) * (scalingCoefficient - 1);
        }
        if (yVertexC > 0) {
            yVertexCNew = yVertexC + Math.abs((yVertexC - yVertexA)) * (scalingCoefficient - 1);
        } else {
            yVertexCNew = yVertexC - Math.abs((yVertexC - yVertexA)) * (scalingCoefficient - 1);
        }

        if (xVertexBNew > 11 || xVertexBNew < -11 ||
            yVertexBNew > 11 || yVertexBNew < -11 ||
            xVertexCNew > 11 || xVertexCNew < -11 ||
            yVertexCNew > 11 || yVertexCNew < -11) {
            document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                += "<li>Завеликий коефіцієнт масштабування, виберіть менший, наприклад: 1.05</li>";
            document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
            return;
        }

        turnAngle = turnAngle * Math.PI / 180;
        xVertexBNew -= xVertexA;
        yVertexBNew -= yVertexA;
        xVertexCNew -= xVertexA;
        yVertexCNew -= yVertexA;
        let xVertexBNewWithTurn = xVertexBNew * Math.cos(turnAngle) - yVertexBNew * Math.sin(turnAngle);
        let yVertexBNewWithTurn = xVertexBNew * Math.sin(turnAngle) + yVertexBNew * Math.cos(turnAngle);
        let xVertexCNewWithTurn = xVertexCNew * Math.cos(turnAngle) - yVertexCNew * Math.sin(turnAngle);
        let yVertexCNewWithTurn = xVertexCNew * Math.sin(turnAngle) + yVertexCNew * Math.cos(turnAngle);
        xVertexBNewWithTurn += xVertexA;
        yVertexBNewWithTurn += yVertexA;
        xVertexCNewWithTurn += xVertexA;
        yVertexCNewWithTurn += yVertexA;

        ctx.beginPath();
        ctx.moveTo((xVertexA + 11) * (gridSize + systemLineWidth),
            Math.abs(yVertexA - 11) * (gridSize + systemLineWidth));
        ctx.lineTo((xVertexBNewWithTurn + 11) * (gridSize + systemLineWidth),
            Math.abs(yVertexBNewWithTurn - 11) * (gridSize + systemLineWidth));
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo((xVertexBNewWithTurn + 11) * (gridSize + systemLineWidth),
            Math.abs(yVertexBNewWithTurn - 11) * (gridSize + systemLineWidth));
        ctx.lineTo((xVertexCNewWithTurn + 11) * (gridSize + systemLineWidth),
            Math.abs(yVertexCNewWithTurn - 11) * (gridSize + systemLineWidth));
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo((xVertexCNewWithTurn + 11) * (gridSize + systemLineWidth),
            Math.abs(yVertexCNewWithTurn - 11) * (gridSize + systemLineWidth));
        ctx.lineTo((xVertexA + 11) * (gridSize + systemLineWidth),
            Math.abs(yVertexA - 11) * (gridSize + systemLineWidth));
        ctx.stroke();

        document.getElementsByClassName("errorMessagesBox")[0].innerHTML = "";
        document.getElementsByClassName("errorMessagesBox")[0].style = "display: none;"
    }

    componentDidMount() {
        this.drawCoordinateSystem(700, 700, 30, 1);
        document.getElementById("scalInput").value = "1";
    }

    render() {
        return (
            <div className="practiceContent">
                <div id="affineTransformationsProperties">
                    <form id="affineTransformationsInputs">
                        <div className="centerText inputBlock">Введіть координати трикутника ABC:</div>
                        Вершина A:<br/>
                        <div>
                            <label className="vertexA inputBlock" id="xInput">
                                Х:
                                <input className="numberInput" placeholder="0"
                                       title="Координата Х знаходиться в проміжку [-10;10]"
                                       onChange={(e) => this.handleChangeXCoordinate(e, 'A')}
                                />
                            </label>
                            <label className="vertexA inputBlock" id="yInput">
                                Y:
                                <input className="numberInput" placeholder="0"
                                       title="Координата Y знаходиться в проміжку [-10;10]"
                                       onChange={(e) => this.handleChangeYCoordinate(e, 'A')}
                                />
                            </label>
                        </div>
                        Вершина B:<br/>
                        <div>
                            <label className="vertexB inputBlock" id="xInput">
                                Х:
                                <input className="numberInput" placeholder="0"
                                       title="Координата Х знаходиться в проміжку [-10;10]"
                                       onChange={(e) => this.handleChangeXCoordinate(e, 'B')}
                                />
                            </label>
                            <label className="vertexB inputBlock" id="yInput">
                                Y:
                                <input className="numberInput" placeholder="0"
                                       title="Координата Y знаходиться в проміжку [-10;10]"
                                       onChange={(e) => this.handleChangeYCoordinate(e, 'B')}
                                />
                            </label>
                        </div>
                        Вершина C:<br/>
                        <div>
                            <label className="vertexC inputBlock" id="xInput">
                                Х:
                                <input className="numberInput" placeholder="0"
                                       title="Координата Х знаходиться в проміжку [-10;10]"
                                       onChange={(e) => this.handleChangeXCoordinate(e, 'C')}
                                />
                            </label>
                            <label className="vertexC inputBlock" id="yInput">
                                Y:
                                <input className="numberInput" placeholder="0"
                                       title="Координата Y знаходиться в проміжку [-10;10]"
                                       onChange={(e) => this.handleChangeYCoordinate(e, 'C')}
                                />
                            </label>
                        </div>
                        <label className="inputBlock" id="inlineInput">
                            <div style={{width: '71%'}}>
                                Введіть коефіцієнт масштабування:
                            </div>
                            <div style={{width: '29%'}} id="specialInput">
                                <input className="numberInput" style={{margin: '0'}} placeholder="0" id="scalInput"
                                       onChange={(e) => this.handleChangeScaling(e)}
                                       title="Обмеження на ввід поля встановлюється в процесі побудови"/>
                            </div>
                        </label>
                        <label className="inputBlock" id="inlineInput">
                            <div style={{width: '71%'}}>
                                Введіть кут, на який трикутник зробить поворот:
                            </div>
                            <div style={{width: '29%'}} id="specialInput">
                                <input type="number" className="numberInput" style={{margin: '0'}} placeholder="0"
                                       title="Кут, на який буде здіснено поворот"
                                       onChange={(e) => this.handleChangeAngleInput(e)}/>
                            </div>
                        </label>
                        <input id='fileInput' type='file' hidden onChange={() => this.handleChooseFileChange()}/>
                        <input type="button" className="affineTransformationsButton" value="Зберегти результат"
                               title="Завантажує файл з результатом побудови"
                               onClick={() => this.handleSaveFileClick()}/>
                        <input type="button" className="affineTransformationsButton" value="Допомога"
                               onClick={() => this.handleHelpButtonClick()}/>
                    </form>
                    <div className="errorMessagesBox" id="affineTransformationsErrors"></div>
                    <div className="helpBox">
                        <p>
                            У даній частині програми ви можете побудувати трикутник за вершинами, координати яких ви
                            вводите у відповідних полях. На кожну координату вершин є обмеження: координата належить
                            проміжку [-10;10].
                        </p>
                        <p>
                            Також ви можете збільшити або зменшити побудований трикутник, вказавши коефіцієнт
                            масштабування. Якщо трикутник не зможе вміститися на координатній площині, програма виведе
                            повідомлення.
                        </p>
                        <p>
                            Також ви можете зробити поворот трикутника відносно вершини А, вказавши кут, який потрібно
                            використати, у відповідному полі. Ви маєте змогу завантажити файл з отриманою побудованою
                            фігурою.
                        </p>
                        <input type="button" className="colorModelsButton" value="Закрити"
                               title="Закриває вікно допомоги"
                               onClick={() => this.handleCloseHelpButtonClick()}/>
                    </div>
                </div>
                <div id="affineTransformationsBuilding">
                    <canvas id="canvasAffineTransformations" width={700} height={700}/>
                </div>
            </div>
        );
    }
}

export default AffineTransformationsPractice;