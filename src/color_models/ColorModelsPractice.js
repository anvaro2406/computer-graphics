import React from 'react';
import './ColorModels.css';

class ColorModelsPractice extends React.Component {
    handleCanvasMouseMove(e) {
        var canvas = document.getElementById(e.target.id);
        var rect = canvas.getBoundingClientRect();
        var ctx = canvas.getContext('2d');
        var pixelData = ctx.getImageData(e.clientX - rect.left, e.clientY - rect.top, 1, 1).data;
        const rgba = `rgba(${pixelData[0]}, ${pixelData[1]}, ${pixelData[2]}, ${pixelData[3] / 255})`;
        document.getElementById('rgbHsvConverterColor').style.background = rgba;

        let rabs, gabs, babs;
        rabs = pixelData[0] / 255;
        gabs = pixelData[1] / 255;
        babs = pixelData[2] / 255;
        let max = Math.max(rabs, gabs, babs);
        let min = Math.min(rabs, gabs, babs);
        var h = 0;
        var s = 0;
        var v = 0;
        if (max === min) {
            h = 0;
        } else if (max === rabs && gabs >= babs) {
            h = 60 * ((gabs - babs) / (max - min)) + 0;
        } else if (max === rabs && gabs < babs) {
            h = 60 * ((gabs - babs) / (max - min)) + 360;
        } else if (max === gabs) {
            h = 60 * ((babs - rabs) / (max - min)) + 120;
        } else if (max === babs) {
            h = 60 * ((rabs - gabs) / (max - min)) + 240;
        }

        if (max === 0) {
            s = 0;
        } else {
            s = (1 - min / max) * 100;
        }

        v = max * 100;

        let result = "RGB(" + pixelData[0] + ", " + pixelData[1] + ", " + pixelData[2] + ")<br/> == <br/>"
            + "HSV(" + Math.round(h) + ", " + Math.round(s) + "%, " + Math.round(v) + "%)";

        document.getElementById('rgbHsvConverterResult').innerHTML = result;
    }

    handleChooseFileClick() {
        var fileInput = document.getElementById('fileInput');
        fileInput.click();
    }

    handleSaveFileClick() {
        var canvas = document.getElementById("afterCanvas");
        const link = document.createElement('a');
        link.download = 'imageAfterChanges.png';
        link.href = canvas.toDataURL();
        link.click();
    }

    handleChooseFileChange() {
        const ctx = document.getElementById("beforeCanvas").getContext('2d');
        const ctx2 = document.getElementById("afterCanvas").getContext('2d');
        var fileInput = document.getElementById("fileInput");
        const [file] = fileInput.files
        var img = new Image();
        img.onload = function () {
            ctx.clearRect(0, 0, 600, 600);
            ctx.drawImage(img, 0, 0, 600, 600);
            ctx2.clearRect(0, 0, 600, 600);
            ctx2.drawImage(img, 0, 0, 600, 600);

        };

        if (file) {
            img.src = window.URL.createObjectURL(file)
        }

        document.getElementById("saturationRange").value = 50;
    }

    handleSaturationChange(e) {
        let beforeCtx = document.getElementById("beforeCanvas").getContext('2d');
        let afterCtx = document.getElementById("afterCanvas").getContext('2d');

        let tmpCanvas = document.createElement('canvas');
        tmpCanvas.width = 600;
        tmpCanvas.height = 600;
        let tmpCanvasCtx = tmpCanvas.getContext('2d');
        tmpCanvasCtx.drawImage(beforeCtx.canvas, 0, 0);
        tmpCanvasCtx.globalCompositeOperation = "saturation";
        tmpCanvasCtx.fillStyle = "hsl(120," + e.target.value + "%,50%)";
        tmpCanvasCtx.fillRect(0, 0, 600, 600);
        tmpCanvasCtx.globalCompositeOperation = "source-over";

        afterCtx.drawImage(tmpCanvasCtx.canvas, 0, 0);
    }

    handleHelpButtonClick() {
        var form = document.getElementById("colorModelsInputs");
        var helpBox = document.getElementsByClassName("helpBox")[0];
        var colorModelsBuilding = document.getElementById("colorModelsBuilding");
        var colorModelsProperties = document.getElementById("colorModelsProperties");
        colorModelsProperties.style = "width: 23%";
        colorModelsBuilding.style = "width: 76%";
        form.style = "display: none;"
        helpBox.style = "display: block;"
    }

    handleCloseHelpButtonClick() {
        var form = document.getElementById("colorModelsInputs");
        var helpBox = document.getElementsByClassName("helpBox")[0];
        var colorModelsBuilding = document.getElementById("colorModelsBuilding");
        var colorModelsProperties = document.getElementById("colorModelsProperties");
        colorModelsProperties.style = "width: 13%";
        colorModelsBuilding.style = "width: 86%";
        form.style = "display: block;"
        helpBox.style = "display: none;"
    }

    componentDidMount() {
        const ctx = document.getElementById("beforeCanvas").getContext('2d');
        const ctx2 = document.getElementById("afterCanvas").getContext('2d');
        var img = new Image();
        img.src = "http://localhost:3000/images/fractals_1.1.jpg";
        img.onload = function () {
            ctx.drawImage(img, 0, 0, 600, 600);
            ctx2.drawImage(img, 0, 0, 600, 600);
        };
    }

    render() {
        return (
            <div className="practiceContent">
                <div id="colorModelsProperties">
                    <form id="colorModelsInputs">
                        <label>
                            Насиченість по зеленому кольору:<br/>
                            <input type="range" min="1" max="100" step="1" id="saturationRange"
                                   onChange={(e) => this.handleSaturationChange(e)}/>
                        </label>

                        <div id="rgbHsvConvert">
                            <span id="rgbHsvConverterColor"></span>
                            <span id="rgbHsvConverterResult">
                                RGB(0, 0, 0)<br/>
                                == <br/>
                                HSV(0, 0%, 0%)
                            </span>
                        </div>
                        <input id='fileInput' type='file' hidden onChange={() => this.handleChooseFileChange()}/>
                        <input type="button" className="colorModelsButton" value="Вибрати файл"
                               onClick={() => this.handleChooseFileClick()}/>
                        <input type="button" className="colorModelsButton" value="Зберегти результат"
                               title="Завантажує файл з результатом побудови"
                               onClick={() => this.handleSaveFileClick()}/>
                        <input type="button" className="colorModelsButton" value="Допомога"
                               onClick={() => this.handleHelpButtonClick()}/>
                    </form>
                    <div className="helpBox">
                        <p>
                            У даній частині програми ви можете змінити насиченість по зеленому кольору, рухаючи повзунок
                            у верхній частині панелі параметрів, для зображення за замовчуванням або для зображення, яке
                            можна завантажити натиснувши кнопку "Вибрати файл". Також ви можете зберегти результуюче
                            зображення, натиснувши відповідну кнопку.
                        </p>
                        <p>
                            Програма дозволяє переглядати значення кольору у двох колірних моделях: RGB та HSV. Для
                            цього потрібно навести курсор миші на будь-яке із зображень наведених справа. Результуючі
                            значення кольору стосується пікселя, на який наведений курсор миші
                        </p>
                        <input type="button" className="colorModelsButton" value="Закрити"
                               title="Закриває вікно допомоги"
                               onClick={() => this.handleCloseHelpButtonClick()}/>
                    </div>
                </div>
                <div id="colorModelsBuilding">
                    <div className="canvasColorModels" id="before">
                        Зображення до змін:
                        <canvas className="canvas" id="beforeCanvas" width={600} height={600}
                                onMouseMove={(e) => this.handleCanvasMouseMove(e)}/>
                    </div>
                    <div className="canvasColorModels" id="after">
                        Зображення після змін:
                        <canvas className="canvas" id="afterCanvas" width={600} height={600}
                                onMouseMove={(e) => this.handleCanvasMouseMove(e)}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default ColorModelsPractice;