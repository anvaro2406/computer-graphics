import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './general_wrapper/App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);