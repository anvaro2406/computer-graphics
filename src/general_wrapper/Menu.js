import React from 'react';
import {Link} from 'react-router-dom';
import './general_wrapper_styles.css';

class Menu extends React.Component {
    render() {
        return (
            <div className="menu">
                <FractalsButton/>
                <ColorModelsButton/>
                <AffineTransformationsButton/>
            </div>
        );
    }
}

class FractalsButton extends React.Component {
    render() {
        return (
            <div className="dropdown themeButton" id="fractals">
                <p id="menuText">Фрактали</p>
                <div className="dropdownContent" id="fractalsDropdownContent">
                    <Link to="/fractals_theory" id="menuText">Теоретичні відомості про фрактали</Link>
                    <Link to="/fractals_practice" id="menuText">Практичні дії над фрактали</Link>
                </div>
            </div>

        );
    }
}

class ColorModelsButton extends React.Component {
    render() {
        return (
            <div className="dropdown themeButton" id="colorModels">
                <p id="menuText">Колірні моделі</p>
                <div className="dropdownContent" id="colorModelsDropdownContent">
                    <Link to="/color_models_theory" id="menuText">Теоретичні відомості про колірні моделі</Link>
                    <Link to="/color_models_practice" id="menuText">Практичні дії над колірними моделями</Link>
                </div>
            </div>
        );
    }
}

class AffineTransformationsButton extends React.Component {
    render() {
        return (
            <div className="dropdown themeButton"  id="affineTransformations">
                <p id="menuText">Афінні перетворення</p>
                <div className="dropdownContent" id="affineTransformationsDropdownContent">
                    <Link to="/affine_transformations_theory" id="menuText">Теоретичні відомості про афінні перетворення</Link>
                    <Link to="/affine_transformations_practice" id="menuText">Практичні дії з афінними перетвореннями</Link>
                </div>
            </div>
        );
    }
}

export default Menu;