import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Home from "../home_page/Home";
import Menu from "./Menu";
import Header from "./Header";
import Footer from "./Footer";
import FractalsTheory from "../fractals/FractalsTheory";
import ColorModelsTheory from "../color_models/ColorModelsTheory";
import AffineTransformationsTheory from "../affine_transformations/AffineTransformationsTheory";
import FractalsPractice from "../fractals/FractalsPractice";
import ColorModelsPractice from "../color_models/ColorModelsPractice";
import AffineTransformationsPractice from "../affine_transformations/AffineTransformationsPractice";


class App extends React.Component  {
    render() {
        return (
            <div className="wrapper">
                <BrowserRouter>
                    <Header/>
                    <Menu/>
                    <Routes>
                        <Route path="/" element={<Home/>}/>
                        <Route path="/fractals_theory" element={<FractalsTheory/>}/>
                        <Route path="/color_models_theory" element={<ColorModelsTheory/>}/>
                        <Route path="/affine_transformations_theory" element={<AffineTransformationsTheory/>}/>
                        <Route path="/fractals_practice" element={<FractalsPractice/>}/>
                        <Route path="/color_models_practice" element={<ColorModelsPractice/>}/>
                        <Route path="/affine_transformations_practice" element={<AffineTransformationsPractice/>}/>
                    </Routes>
                </BrowserRouter>
                <Footer/>
            </div>
        );
    }
}

export default App;