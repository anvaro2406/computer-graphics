import React from 'react';
import "./general_wrapper_styles.css";

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <p id="headerFooterText">&copy; Андрій Романов</p>
            </div>
        );
    }
}

export default Footer;