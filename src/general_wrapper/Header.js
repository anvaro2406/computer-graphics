import React from 'react';
import {Link} from "react-router-dom";
import "./general_wrapper_styles.css";

class Header extends React.Component  {
    render() {
        return (
            <Link to="/">
                <div className="header">
                    <p id="headerFooterText">Комп'ютерна графіка</p>
                </div>
            </Link>
        );
    }
}

export default Header;