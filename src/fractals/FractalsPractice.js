import React from 'react';
import './Fractals.css';
import TFractals from "./TFractals";
import HFractals from "./HFractals";

class FractalsPractice extends React.Component  {
    constructor(props) {
        super(props);
        this.state = {
            fractalType: String,
        };
    }

    handleClickTFractal() {
        this.setState({fractalType: <TFractals/>});
    }

    handleClickHFractal() {
        this.setState({fractalType: <HFractals/>});
    }

    renderFractal(){
        return (
            this.state.fractalType
        );
    }

    render() {
        return (
            <div className="practiceContent">
                <div id="fractalsList">
                    <ul>
                        <li onClick={() => this.handleClickTFractal()}>Т-фрактал</li>
                        <li onClick={() => this.handleClickHFractal()}>Н-фрактал</li>
                    </ul>
                </div>
                <div id="fractalsBuilding">
                    {this.renderFractal()}
                </div>
            </div>
        );
    }
}

export default FractalsPractice;