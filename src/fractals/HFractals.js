import React from 'react';
import './Fractals.css';

class FractalsPractice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            iterations: 0,
            centerX: 0,
            centerY: 0,
            styleType: '',
            displayStatusErrors: false
        };

        this.handleChangeIterations = this.handleChangeIterations.bind(this);
        this.handleChangeXCoordinate = this.handleChangeXCoordinate.bind(this);
        this.handleChangeYCoordinate = this.handleChangeYCoordinate.bind(this);
        this.handleChangeSelect = this.handleChangeSelect.bind(this);
        this.handleSaveFileClick = this.handleSaveFileClick.bind(this);
        this.handleHelpButtonClick = this.handleHelpButtonClick.bind(this);
        this.handleCloseHelpButtonClick = this.handleCloseHelpButtonClick.bind(this);
    }

    handleChangeIterations(e) {
        this.setState({iterations: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
        if (e.target.value > 10 || e.target.value < 0) {
            document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                += "<li>Число " + e.target.value + " не належить проміжку [0;10]</li>";
            document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
            return;
        }

        this.updateCanvas(
            isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
            this.state.centerX,
            this.state.centerY,
            this.state.styleType
        );
    }

    handleChangeXCoordinate(e) {
        this.setState({centerX: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
        if (e.target.value > 300 || e.target.value < -300) {
            document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                += "<li>Число " + e.target.value + " не належить проміжку [-300;300]</li>";
            document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
            return;
        }
        this.updateCanvas(
            this.state.iterations,
            isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
            this.state.centerY,
            this.state.styleType
        );
    }

    handleChangeYCoordinate(e) {
        this.setState({centerY: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
        if (e.target.value > 300 || e.target.value < -300) {
            document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                += "<li>Число " + e.target.value + " не належить проміжку [-300;300]</li>";
            document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;"
            return;
        }
        this.updateCanvas(
            this.state.iterations,
            this.state.centerX,
            isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
            this.state.styleType
        );
    }

    handleChangeSelect(e) {
        this.setState({styleType: e.target.value});
        this.updateCanvas(this.state.iterations, this.state.centerX, this.state.centerY, e.target.value);
    }

    handleSaveFileClick() {
        var canvas = document.getElementById("canvas");
        const link = document.createElement('a');
        link.download = 'fractalImage.png';
        link.href = canvas.toDataURL();
        link.click();
    }

    handleHelpButtonClick() {
        var form = document.getElementsByClassName("inputFields")[0];
        var errors = document.getElementsByClassName("errorMessagesBox")[0];
        var helpBox = document.getElementsByClassName("helpBox")[0];
        if (errors.style.getPropertyValue("display") === "block") {
            this.setState({displayStatusErrors: true});
        } else {
            this.setState({displayStatusErrors: false});
        }
        errors.style = "display: none;"
        form.style = "display: none;"
        helpBox.style = "display: block;"
    }

    handleCloseHelpButtonClick() {
        var form = document.getElementsByClassName("inputFields")[0];
        var helpBox = document.getElementsByClassName("helpBox")[0];
        var errors = document.getElementsByClassName("errorMessagesBox")[0];
        if (this.state.displayStatusErrors) {
            errors.style = "display: block;"
        } else {
            errors.style = "display: none;"
        }
        form.style = "display: block;"
        helpBox.style = "display: none;"
    }

    componentDidMount() {
        this.updateCanvas(0, 0, 0, "blackWhite");
    }

    updateCanvas(iter, x, y, styleT) {
        var iterations = this.state.iterations;
        var centerX = this.state.centerX;
        var centerY = this.state.centerY;
        var styleType = this.state.styleType;

        if (iter !== iterations) {
            iterations = iter;
        }
        if (x !== centerX) {
            centerX = x;
        }
        if (y !== centerY) {
            centerY = y;
        }
        if (styleT !== styleType) {
            styleType = styleT;
        }

        if (iterations > 10 || iterations < 0) {
            return;
        } else if (centerX > 300 || centerX < -300) {
            return;
        } else if (centerY > 300 || centerY < -300) {
            return;
        }

        var centerDiff = 300;
        var maxLength = 600;

        centerX += 300;
        centerY = Math.abs(centerY - 300);

        const ctx = document.getElementById("canvas").getContext('2d');
        if (styleType === "redBlack") {
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, 600, 600);
            ctx.strokeStyle = "red";
        } else if (styleType === "blueYellow") {
            ctx.fillStyle = "yellow";
            ctx.fillRect(0, 0, 600, 600);
            ctx.strokeStyle = "blue";
        } else {
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, 600, 600);
            ctx.strokeStyle = "white";
        }

        for (let i = 0; i < iterations; i++) {
            let currentLength = (maxLength / Math.pow(2, (i + 1)));

            let verticalLinesCountRow = Math.pow(2, i);
            let verticalLinesCountCol = Math.pow(2, i + 1);

            for (let j = 0; j < verticalLinesCountRow; j++) {
                let currentY = (currentLength / 2) + 2 * currentLength * j + centerY - centerDiff;

                for (let c = 0; c < verticalLinesCountCol; c++) {
                    let currentX = (currentLength / 2) + currentLength * c + centerX - centerDiff;
                    ctx.beginPath();
                    ctx.moveTo(currentX, currentY);
                    ctx.lineTo(currentX, currentY + currentLength);
                    ctx.stroke();
                }
            }

            let horizontalLinesCountRow = Math.pow(2, i);
            let horizontalLinesCountCol = Math.pow(2, i);

            for (let j = 0; j < horizontalLinesCountRow; j++) {
                let currentY = currentLength + 2 * currentLength * j + centerY - centerDiff;

                for (let c = 0; c < horizontalLinesCountCol; c++) {
                    let currentX = (currentLength / 2) + 2 * currentLength * c + centerX - centerDiff;
                    ctx.beginPath();
                    ctx.moveTo(currentX, currentY);
                    ctx.lineTo(currentX + currentLength, currentY);
                    ctx.stroke();
                }
            }
        }
        document.getElementsByClassName("errorMessagesBox")[0].innerHTML = "";
        document.getElementsByClassName("errorMessagesBox")[0].style = "display: none;"
    }

    render() {
        return (
            <div>
                <div id="fractalsProperties">
                    <form className="inputFields">
                        <label className="inputField">
                            Введіть кількість ітерацій:
                            <input className="numberInput" placeholder="0"
                                   title="Кількість ітерацій знаходяться в проміжку [0;10]"
                                   onChange={(e) => this.handleChangeIterations(e)}/>
                        </label>
                        Введіть координати центру фракталу.<br/>
                        <div>
                            <label className="inputField" id="xInput">
                                Х:
                                <input className="numberInput" placeholder="0"
                                       title="Координата Х знаходиться в проміжку [-300;300]"
                                       onChange={(e) => this.handleChangeXCoordinate(e)}/>
                            </label>
                            <label className="inputField" id="yInput">
                                Y:
                                <input className="numberInput" placeholder="0"
                                       title="Координата Y знаходиться в проміжку [-300;300]"
                                       onChange={(e) => this.handleChangeYCoordinate(e)}/>
                            </label>
                        </div>
                        <label className="inputField">
                            Виберіть стиль, який буде використаний:
                            <select className="fractals" id="styleSelect" onChange={(e) => this.handleChangeSelect(e)}>
                                <option value="blackWhite" defaultChecked>Чорно-білий стиль</option>
                                <option value="blueYellow">Синьо-жовтий стиль</option>
                                <option value="redBlack">Червоно-чорний стиль</option>
                            </select>
                        </label>
                        <input type="button" className="fractalsButtons" value="Зберегти результат"
                               title="Завантажує файл з результатом побудови"
                               onClick={() => this.handleSaveFileClick()}/>
                        <input type="button" className="fractalsButtons" value="Допомога"
                               onClick={() => this.handleHelpButtonClick()}/>
                    </form>
                    <div className="errorMessagesBox"></div>
                    <div className="helpBox">
                        <p>
                            Для коректного виконання побудови фракталу, введіть кількість ітерацій, впродовж яких
                            будується фрактал, зауважте, що кількість ітерацій належить проміжку [0;10].
                        </p>
                        <p>
                            Крім того введіть координати центру фракталу у відповідні поля, зауважте, що координати
                            належать проміжку [-300;300]. За замовчуванням координати центру (0;0).
                        </p>
                        <p>
                            Ви можете завантажити результат виконання останньої побудови, яку ви бачете на полі справа,
                            натиснувши відповідну кнопку. Також ви маєте можливість змінити кольори, з допомогою яких
                            будується фрактал, для цього виберіть бажаний стиль у випадаючому списку.
                        </p>
                        <input type="button" className="fractalsButtons" value="Закрити" title="Закриває вікно допомоги"
                               onClick={() => this.handleCloseHelpButtonClick()}/>
                    </div>
                </div>
                <div id="canvasFractal">
                    <canvas id="canvas" width={600} height={600}/>
                </div>
            </div>
        );
    }
}

export default FractalsPractice;