import React from 'react';
import './Fractals.css';

class FractalsPractice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            iterations: 0,
            centerX: 0,
            centerY: 0,
            styleType: '',
            displayStatusErrors: false
        };

        this.handleChangeIterations = this.handleChangeIterations.bind(this);
        this.handleChangeXCoordinate = this.handleChangeXCoordinate.bind(this);
        this.handleChangeYCoordinate = this.handleChangeYCoordinate.bind(this);
        this.handleChangeSelect = this.handleChangeSelect.bind(this);
        this.handleSaveFileClick = this.handleSaveFileClick.bind(this);
        this.handleHelpButtonClick = this.handleHelpButtonClick.bind(this);
        this.handleCloseHelpButtonClick = this.handleCloseHelpButtonClick.bind(this);
    }

    handleChangeIterations(e) {
        this.setState({iterations: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
        if (e.target.value > 10 || e.target.value < 0) {
            let errors = document.getElementsByClassName("errorMessagesBox")[0];
            errors.innerHTML += "<li>Число " + e.target.value + " не належить проміжку [0;10]</li>";
            errors.style = "display: block;";
            return;
        }
        this.updateCanvas(
            isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
            this.state.centerX,
            this.state.centerY,
            this.state.styleType
        );
    }

    handleChangeXCoordinate(e) {
        this.setState({centerX: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
        if (e.target.value > 300 || e.target.value < -300) {
            let errors = document.getElementsByClassName("errorMessagesBox")[0];
            errors.innerHTML += "<li>Число " + e.target.value + " не належить проміжку [-300;300]</li>";
            errors.style = "display: block;";
            return;
        }
        this.updateCanvas(
            this.state.iterations,
            isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
            this.state.centerY,
            this.state.styleType
        );
    }

    handleChangeYCoordinate(e) {
        this.setState({centerY: isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value)});
        if (e.target.value > 300 || e.target.value < -300) {
            document.getElementsByClassName("errorMessagesBox")[0].innerHTML
                += "<li>Число " + e.target.value + " не належить проміжку [-300;300]</li>";
            document.getElementsByClassName("errorMessagesBox")[0].style = "display: block;";
            return;
        }
        this.updateCanvas(
            this.state.iterations,
            this.state.centerX,
            isNaN(Number.parseInt(e.target.value)) ? 0 : Number.parseInt(e.target.value),
            this.state.styleType
        );
    }

    handleChangeSelect(e) {
        this.setState({styleType: e.target.value});
        this.updateCanvas(this.state.iterations, this.state.centerX, this.state.centerY, e.target.value);
    }

    handleSaveFileClick() {
        var canvas = document.getElementById("canvas");
        const link = document.createElement('a');
        link.download = 'fractalImage.png';
        link.href = canvas.toDataURL();
        link.click();
    }

    handleHelpButtonClick() {
        var form = document.getElementsByClassName("inputFields")[0];
        var errors = document.getElementsByClassName("errorMessagesBox")[0];
        var helpBox = document.getElementsByClassName("helpBox")[0];
        if (errors.style.getPropertyValue("display") === "block") {
            this.setState({displayStatusErrors: true});
        } else {
            this.setState({displayStatusErrors: false});
        }
        errors.style = "display: none;";
        form.style = "display: none;";
        helpBox.style = "display: block;";
    }

    handleCloseHelpButtonClick() {
        var form = document.getElementsByClassName("inputFields")[0];
        var helpBox = document.getElementsByClassName("helpBox")[0];
        var errors = document.getElementsByClassName("errorMessagesBox")[0];
        if (this.state.displayStatusErrors) {
            errors.style = "display: block;";
        } else {
            errors.style = "display: none;";
        }
        form.style = "display: block;";
        helpBox.style = "display: none;";
    }

    componentDidMount() {
        this.updateCanvas(0, 0, 0, "blackWhite");
    }

    updateCanvas(iter, x, y, styleT) {
        var iterations = this.state.iterations;
        var centerX = this.state.centerX;
        var centerY = this.state.centerY;
        var styleType = this.state.styleType;

        if (iter !== iterations) {
            iterations = iter;
        }
        if (x !== centerX) {
            centerX = x;
        }
        if (y !== centerY) {
            centerY = y;
        }
        if (styleT !== styleType) {
            styleType = styleT;
        }

        if (iterations > 10 || iterations < 0) {
            return;
        } else if (centerX > 300 || centerX < -300) {
            return;
        } else if (centerY > 300 || centerY < -300) {
            return;
        }

        centerX += 300;
        centerY = Math.abs(centerY - 300);

        const ctx = document.getElementById("canvas").getContext('2d');
        var maxWidth = 600;
        var maxHeight = 600;
        if (styleType === "redBlack") {
            ctx.fillStyle = "red";
            ctx.fillRect(0, 0, maxWidth, maxHeight);
            ctx.fillStyle = "black";
        } else if (styleType === "blueYellow") {
            ctx.fillStyle = "yellow";
            ctx.fillRect(0, 0, maxWidth, maxHeight);
            ctx.fillStyle = "blue";
        } else {
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, maxWidth, maxHeight);
            ctx.fillStyle = "white";
        }

        for (let i = 0; i < iterations; i++) {
            let currentWidth = (maxWidth / Math.pow(2, (i + 1)));
            let currentHeight = (maxHeight / Math.pow(2, (i + 1)));
            let rowsCount = Math.pow(2, i);
            let colsCount = Math.pow(2, i);

            for (let j = 0; j < rowsCount; j++) {
                let currentY = (currentHeight / 2) + 2 * currentHeight * j + centerY - 300;

                for (let c = 0; c < colsCount; c++) {
                    let currentX = (currentWidth / 2) + 2 * currentWidth * c + centerX - 300;
                    ctx.fillRect(currentX, currentY, currentWidth, currentHeight);
                }
            }
        }
        document.getElementsByClassName("errorMessagesBox")[0].innerHTML = "";
        document.getElementsByClassName("errorMessagesBox")[0].style = "display: none;"
    }

    render() {
        return (
            <div>
                <div id="fractalsProperties">
                    <form className="inputFields">
                        <label className="inputField">
                            Введіть кількість ітерацій:
                            <input className="numberInput" placeholder="0"
                                   title="Кількість ітерацій знаходяться в проміжку [0;10]"
                                   onChange={(e) => this.handleChangeIterations(e)}/>
                        </label>
                        <div>
                            Введіть координати центру фракталу.<br/>
                            <label className="inputField" id="xInput">
                                Х:
                                <input className="numberInput" placeholder="0"
                                       title="Координата Х знаходиться в проміжку [-300;300]"
                                       onChange={(e) => this.handleChangeXCoordinate(e)}/>
                            </label>
                            <label className="inputField" id="yInput">
                                Y:
                                <input className="numberInput" placeholder="0"
                                       title="Координата Y знаходиться в проміжку [-300;300]"
                                       onChange={(e) => this.handleChangeYCoordinate(e)}/>
                            </label>
                        </div>
                        <label className="inputField">
                            Виберіть стиль, який буде використаний:
                            <select className="fractals" id="styleSelect" onChange={(e) => this.handleChangeSelect(e)}>
                                <option value="blackWhite" defaultChecked>Чорно-білий стиль</option>
                                <option value="blueYellow">Синьо-жовтий стиль</option>
                                <option value="redBlack">Червоно-чорний стиль</option>
                            </select>
                        </label>
                        <input type="button" className="fractalsButtons" value="Зберегти результат"
                               title="Завантажує файл з результатом побудови"
                               onClick={() => this.handleSaveFileClick()}/>
                        <input type="button" className="fractalsButtons" value="Допомога"
                               onClick={() => this.handleHelpButtonClick()}/>
                    </form>
                    <div className="errorMessagesBox"></div>
                    <div className="helpBox">
                        <p>
                            Для коректного виконання побудови фракталу, введіть кількість ітерацій, впродовж яких
                            будується фрактал, зауважте, що кількість ітерацій належить проміжку [0;10].
                        </p>
                        <p>
                            Крім того введіть координати центру фракталу у відповідні поля, зауважте, що координати
                            належать проміжку [-300;300]. За замовчуванням координати центру (0;0).
                        </p>
                        <p>
                            Ви можете завантажити результат виконання останньої побудови, яку ви бачете на полі справа,
                            натиснувши відповідну кнопку. Також ви маєте можливість змінити кольори, з допомогою яких
                            будується фрактал, для цього виберіть бажаний стиль у випадаючому списку.
                        </p>
                        <input type="button" className="fractalsButtons" value="Закрити" title="Закриває вікно допомоги"
                               onClick={() => this.handleCloseHelpButtonClick()}/>
                    </div>
                </div>
                <div id="canvasFractal">
                    <canvas id="canvas" width={600} height={600}/>
                </div>
            </div>
        );
    }
}

export default FractalsPractice;