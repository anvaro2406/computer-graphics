import React from 'react';
import './Fractals.css';

class FractalsTheory extends React.Component {
    render() {
        return (
            <div className="content">
                <div className="center">Поняття про фрактальну графіку.</div>
                <p>
                    Слово фрактал утворено від латинського fractus і в перекладі означає той, що складається з
                    фрагментів (подрібнений). Термін “фрактал”запропоноване Бенуа Мандельбротом в 1975 році для
                    позначення самоподібних структур. Народження фрактальної геометрії прийнято пов'язувати з виходом
                    в 1977 році книги Мандельброта `The Fractal Geometry of Nature'. Фракталом називається структура,
                    що складається з частин, які в якомусь сенсі подібні до цілого. В основі цього явища лежить дуже
                    проста ідея : нескінчену по красі і різноманітності множину фігур можна отримати з відносно простих
                    конструкцій за допомогою всього двох операцій - копіювання і масштабування.
                </p>
                <div className="center">
                    <img src="http://localhost:3000/images/fractals_1.1.jpg" alt="fractals 1.1"/>
                    <img src="http://localhost:3000/images/fractals_1.2.jpg" alt="fractals 1.2"/>
                </div>
                <div className="center">
                    Рис. 1. Приклади фракталів у природі
                </div>
                <p>
                    Фрактали знаходять все більше й більше практичних застосувань у науці. Основна причина цього полягає
                    в тому, що вони описують реальний світ іноді навіть краще, ніж традиційна фізика чи математика.
                    Більшість просторових систем у природі є нерегулярним і фрагментарним, форма цих систем погано
                    піддається опису апаратом евклідової геометрії. Масштабна інваріантність, що спостерігається
                    у фракталах, може бути або точною, або наближеною.
                    Інваріантність - незмінність якої-небудь величини по відношенню до деяких перетворень.
                    Однією з основних властивостей фракталів є самоподібність. У найпростішому випадку невелика частина
                    фрактала містить інформацію про весь фрактал. Серед важливих властивостей фракталів з точки зору
                    програмної реалізації ітераційність та рекурсивність фрактальних структур.
                    Фактично знайдений спосіб легкого представлення складних об'єктів, які схожі на природні. Тобто за
                    допомогою декількох коефіцієнтів можна задати лінії і поверхні дуже складної форми. Фрактальна
                    геометрія незамінна при генерації штучних хмар, гір, поверхні морів тощо.
                    Фрактальна графіка, як і векторна, заснована на математичних обчисленнях. Однак, базовим елементом є
                    математична формула, ніяких об'єктів у пам'яті комп'ютера не зберігається і зображення будується
                    виключно за формулами,рівняннями.
                </p>
                <div className="center">Класифікація фракталів може бути проведена за різними критеріями.</div>
                <p>Найчастіше використовують класифікацію за способом побудови</p>
                <ul>
                    <li>Геометричні (конструктивні);</li>
                    <li>Алгебраїчні (рекурентні, динамічні);</li>
                    <li>Стохастичні (випадкові)</li>
                    <li>IFS-фрактали</li>
                </ul>
                <p>
                    Геометричні та алгебраїчні фрактали називають детермінованими. Окремо розглядають підвид фракталів,
                    що утворюється через систему ітеративних функцій (Iterated Functions System) – IFS-фрактали.
                    Крім вище наведеної класифікації, можна навести поділ фракталів за рівнем самоподібності. За цим
                    критерієм виділяють три типи фракталів:
                </p>
                <ul>
                    <li>фрактали точної самоподібності;</li>
                    <li>фрактали з майже самоподібністю;</li>
                    <li>фрактали з статистичною самоподібністю.</li>
                </ul>
                <p>
                    Фракталами точної самоподібності є геометричні фрактали, масштабна інваріантність яких є у явному
                    вигляді. До фракталів з майже самоподібністю належать алгебраїчні фрактали. Майже самоподібність
                    означає, що частинки фракталу у певному сенсі (за деяким законом) подібні до цілого зображення.
                    Статистична самоподібність використовує випадкові величини.
                    Ще одна класифікація враховує поняття детермінованості. До детермінованих фракталів належать
                    геометричні, алгебраїчні фрактали, а до недетермінованих - стохастичні.
                </p>
                <div className="center">Геометричні фрактали</div>
                <p>
                    Фрактали цього типу будуються поетапно. Спочатку зображується основа . Потім деякі частини основи
                    замінюються на фрагмент . На кожному наступному етапі частини вже побудованої фігури , аналогічні
                    заміненим частинам основи , знову замінюються на фрагмент , взятий у відповідному масштабі. Кожного
                    разу масштаб зменшується. Коли зміни стають візуально непомітними , вважають , що побудована фігура
                    добре наближає фрактал і дає уявлення про його форму . Однак насправді для отримання фрактала
                    потрібно нескінченне число етапів. Міняючи основу і фрагмент , можна отримати багато різних
                    геометричних фракталів .У двомірному випадку їх отримують за допомогою деякої ламаної (або
                    поверхні в тривимірному випадку), яку наз. генератором.
                    За один крок алгоритму кожен з відрізків, складових ламаної, замінюється на ламану-генератор, у
                    відповідному масштабі. У результаті повторення цієї процедури, виходить геометричний фрактал.
                    Найвідоміші фрак тали такого типу - Сніжинка Коха, крива Коха, крива Гільберта-Пеано, множина
                    Кантора, килим Серпінського, трикутник Серпінського, крива дракона, Т-Квадрат, губка Менгера…
                </p>
                <div className="center">
                    <img src="http://localhost:3000/images/fractals_2.jpg" alt="fractals 2"/>
                </div>
                <div className="center">Рис. 2. Кроки побудови сніжинки Коха</div>
                <br/>
                <div className="center">
                    <img src="http://localhost:3000/images/fractals_3.gif" alt="fractals 3"/>
                </div>
                <div className="center">Рис. 3. Килим Серпінського</div>
                <p>
                    Цей фрактал є межею нескінченної конструкції, що починається з трикутника та доповнюється
                    рекурсивною заміною кожного сегменту набором із чотирьох сегментів, які утворюють трикутний
                    «виступ».
                </p>
                <div className="center">Теорія динамічних систем - основа алгебраїчних фракталі</div>
                <p>
                    Алгебраїчні фрактали належать до найбільшої групи фракталів. Отримують їх за допомогою нелінійних
                    процесів в n-мірних просторах. Для побудови алгебраїчних фракталів використовуються ітерації
                    нелінійних відображень, що задаються простими алгебраїчними формулами. Найбільш вивчені двомірні
                    процеси. Інтерпретуючи нелінійний ітераційний процес, як дискретну динамічну систему, можна
                    користуватися термінологією теорії цих систем: фазовий портрет, сталий процес, аттрактор і т.д.
                    Дискретною динамічною системою є система, що має визначені стани в конкретні моменти часу
                </p>
                <p>
                    Відомо, що нелінійні динамічні системи володіють декількома стійкими станами. Той стан, в якому
                    опинилася динамічна система після деякого числа ітерацій, залежить від її початкового стану. Тому
                    кожен стійкий стан (або як говорять – аттрактор) володіє деякою областю початкових станів, з яких
                    система обов’язково потрапить в дані кінцеві стани. Таким чином, фазовий простір системи
                    розбивається на області тяжіння аттракторів. Якщо фазовим є двомірний простір, то забарвлюючи
                    області тяжіння різними кольорами, можна отримати колірний фазовий портрет цієї системи
                    (ітераційного процесу). Міняючи алгоритм вибору кольору, можна отримати складні фрактальні
                    картини з химерними багатоколірними узорами. Алгебраїчні фрактали визначаються рекурентним
                    відношенням в кожній точці простору (площина комплексних чисел).
                </p>
                <div className="center">Стохастичні фрактали</div>
                <p>
                    Такі фрактали утворюються в тому випадку, якщо в ітераційному процесі випадковим чином міняти
                    які-небудь його параметри. При цьому виходять об'єкти дуже схожі на природні – несиметричні дерева,
                    порізані берегові лінії, рельєф місцевості і поверхні моря і т.д.
                </p>
                <div className="center">
                    <img src="http://localhost:3000/images/fractals_4.jpg" alt="fractals 4"/>
                </div>
                <div className="center">Рис. 4. Приклад стохастичного фракталу на основі множини Жюліа</div>
                <p>
                    Представниками цього класу можна вважати траекторію броунівського руху, різні види рандомізованих
                    фракталів, тобто таких, які можна отримати за допомогою рекурсивної процедури, в яку на кожному
                    кроці введений випадковий параметр. Змоделювати броунівський рух на комп'ютері досить просто:
                    частинку потрібно зміщувати на задану відстань (у моделі однакових кроків) в довільному напрямку.
                    Спроможність фрактальної графіки застосовують для автоматичної генерації незвичайних ілюстрацій.
                    Типовий представник даного класу фракталів «Плазма».
                </p>
                <div className="center">
                    <img src="http://localhost:3000/images/fractals_5.jpg" alt="fractals 5"/>
                </div>
                <div className="center">Рис. 5. Фрактал «Плазма»</div>
                <p>
                    Для його побудови використовують прямокутник і для кожного його кута визначається колір. Далі
                    знаходиться центральна точка прямокутника і розфарбовується у колір рівний середньому арифметичному
                    кольорів по кутах прямокутника плюс деяке випадкове число. Чим більше випадкове число - тим більш
                    «рваним» буде малюнок. Якщо, наприклад, вважати, що колір точки - це висота над рівнем моря, то
                    отримаєтьтся замість плазми - гірський масив. Саме на цьому принципі моделюються гори в більшості
                    програм. Гранична крива рандомізованої сніжинки Коха може служити моделлю, наприклад, контуру хмари
                    або острова. Поверхня гір моделюється з використанням фракталів.Початок з трикутника в тривимірному
                    просторі та з'єднання центральних точок кожного ребра відрізками, отримуючи 4 трикутники. Далі
                    центральні точки зсуваються догори або донизу на випадкову відстань у фіксованому діапазоні.
                    Процедура повторюється зі зменшенням діапазону на кожній ітерації вдвічі.
                    Рекурсивна природа алгоритму гарантує, що ціле є статистично подібним до кожної з деталей.
                </p>
            </div>
        );
    }
}

export default FractalsTheory;